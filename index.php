<?php

require_once('Animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "nama = ".$sheep->name."<br>"; // "shaun"
echo "Legs = ".$sheep->legs."<br>"; // 4
echo "cold_blooded = ".$sheep->cold_blooded."<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
echo "<br>";

$kodok = new frog("buduk");
echo "nama = ".$kodok->name."<br>"; // "shaun"
echo "Legs = ".$kodok->legs."<br>"; // 4
echo "cold_blooded = ".$kodok->cold_blooded."<br>"; // "no"
$kodok->jump() ; // "hop hop"


echo "<br><br>";
$sungokong = new ape("kera sakti");

echo "nama = ".$sungokong->name."<br>"; // "shaun"
echo "Legs = ".$sungokong->legs."<br>"; // 4
echo "cold_blooded = ".$sungokong->cold_blooded."<br>"; // "no"
$sungokong->yell(); // "Auooo"




?>